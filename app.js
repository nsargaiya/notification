const express = require('express')
const { Sequelize } = require('sequelize');
const app = express()
const PORT = 3000
// adding DB
const db = require('./config/database')
// const bodyParser = require('body-parser')


app.use(express.urlencoded({ extended: true}));

app.use(express.json())

app.use('/', require('./src/routes/notifications'));



app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})