const sequelize = require('sequelize')
const db = require('../../config/database');
const {User,Seen,Type} = require('../models')

const Notification = db.define('notifications',{
    id : {
        type: sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,

    },
    to_user :{
        type:sequelize.INTEGER,
        references: {
            model: User, 
            key: 'id'
          }
    },
    from_user :{
        type:sequelize.INTEGER,
        references: {
            model: User, 
            key: 'id'
          }
    },
    msg :{
        type:sequelize.STRING,
        
    },
    type :{
        type:sequelize.INTEGER,
        references: {
            model: Type, 
            key: 'id'
          }
    },
    seen :{
        type:sequelize.INTEGER,
        references: {
            model: Seen, 
            key: 'id'
          }
    },   
    delete_from_user :{
        type:sequelize.BOOLEAN,
        
    },
    delete_to_user :{
        type:sequelize.BOOLEAN,
        
    },

}, {
    freezeTableName: true // Model tableName will be the same as the model name
  });


module.exports = Notification;