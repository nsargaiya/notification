const sequelize = require('sequelize')
const db = require('../../config/database');


const Seen = db.define('seen',{
    id : {
        type: sequelize.INTEGER,
        primaryKey: true,

    },
    state :{
        type:sequelize.STRING,
        
    },


    deletedAt :{
        type:sequelize.DATE,
        
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
  });

module.exports = Seen;