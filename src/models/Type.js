const sequelize = require('sequelize')
const db = require('../../config/database');


const Type = db.define('type',{

    id : {
        type: sequelize.INTEGER,
        primaryKey: true,
    },
    notification_type :{
        type:sequelize.STRING,
        
    },
    deletedAt :{
        type:sequelize.DATE,
        
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
  });

module.exports = Type;