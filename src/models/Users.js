const sequelize = require('sequelize')
const db = require('../../config/database');


const User = db.define('users',{

    id : {
        type: sequelize.INTEGER,
        primaryKey: true,
    },
    name :{
        type:sequelize.STRING
    },
    active:
    {
        type:sequelize.BOOLEAN
    }


}, {
    freezeTableName: true // Model tableName will be the same as the model name
  });


module.exports = User;