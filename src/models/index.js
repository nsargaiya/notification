const User = require("./Users");
const Notification = require("./Notifications");
const Seen = require("./Seen");
const Type = require("./Type");

let db = require("../../config/database");

db = {...db,User,Notification,Seen,Type};
// console.log(db.Notification)

db.Notification.belongsTo(db.User,{
                                        foreignKey: 'from_user',
                                        as: 'FromId', // Changes applied here
                                    });
db.Notification.belongsTo(db.User,{
                                        foreignKey: 'to_user',
                                        as: 'ToId', // Changes applied here
                                    });
db.Notification.belongsTo(db.Seen,{
                                    foreignKey: 'seen',
                                    as: 'SeenId', // Changes applied here
                                });
db.Notification.belongsTo(db.Type,{
                                    foreignKey: 'type',
                                    as: 'TypeId', // Changes applied here
                                });
// db.Seen.hasMany(db.Notification);
// db.Type.hasMany(db.Notification);
// db.User.hasMany(db.Notification);
// db.User.hasMany(db.Notification);


module.exports = {User,Notification,Seen,Type};