const express = require('express');
const router = express.Router();

const notification = require('../services/notifications-service');
const mailer = require('../services/mail-service');
// const transporter = require('../services/mail-service');


router.get('/notifications/web/:user_id', notification.showNotifications);
router.post('/notifications/:user_id', notification.addNotification);

router.put('/notifications/web/:user_id/:noti_id',notification.updateNotificationStatus);

router.delete('/notifications/web/:user_id/:noti_id',notification.deleteNotifications)


module.exports = router;