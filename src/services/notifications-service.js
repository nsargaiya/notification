const express = require('express');
// const { result } = require('lodash');
const _ = require('lodash'); 
const {Notification,User,Seen,Type}= require("../models/index")
const { sequelize } = require('../models/Users')
const mailer = require('../services/mail-service');


/**
 * Fetch Notifications .
 * @constructor
 * @param {object} req - request object
 * @param {object} res - response object
 */
showNotifications = function (req,res)
{
    let userId = req.params.user_id;
    Notification.findAll(
       {
        attributes: ['id','msg','createdAt'],
        where:{'to_user':userId },
        include:[{model:User , as: 'FromId', attributes: ['name']},
                 {model:Seen , as: 'SeenId', attributes: ['state']},
                 {model:Type , as: 'TypeId', attributes: ['notification_type']}] 
    }).then(
         (data)=>{
            //  console.log(data);
             if(data.length>0)
                 return res.json(data)
             return res.json({msg:"No Notifications"})
            })
        .catch((err)=>{
            res.status(400)
            return res.json(err)
        });
  
}

/**
 * Add Notifications .
 * @constructor
 * @param {object} req - request object
 * @param {object} res - response object
 */
// Add notification to database 
addNotification = function (req,res)
{
    // console.log(req.body);

    let queryData = req.body;

    // fetch id of from user
    User.findAll({
        attributes: ['id','name'],
        where:{
                name:[queryData.from,queryData.to]
                // name:queryData.to
              }
    }).then(
        function success(result)
        {
            let fromUserId,toUserId;
            if(result.length>0)
            {
                // Fetch both user's user id
                for(let value of result)
                {
                    //pick user id and assign them to respective variable
                    if(queryData.from==value["dataValues"]["name"])
                       fromUserId=value["dataValues"]["id"];
             
                    if (queryData.to==value["dataValues"]["name"])
                        toUserId=value["dataValues"]["id"]
                }
                
            }
          
            // both users doesn't exists 
            else
            {

                res.status(400)
                return res.json({msg:`No data found` })
                
            }
     
            // Invalid from user
            if(fromUserId===undefined  )
            {
                res.status(400)
                return res.json({msg:`${queryData.from} user not found` })
            }

            // Invalid To user
            if(toUserId===undefined  )
            {
                res.status(400)
                return res.json({msg:`${queryData.to} user not found` })
            }

                queryData.to_user = toUserId;
                queryData.from_user = fromUserId;          
               
                //adding to database (email)
                queryData.type=1; // type 1 is email
                queryData.seen=3; // Seen 3 is unsent
                
                mailerResponse =  mailer.sendEmail(queryData);
                mailerResponse.then(
                    function success(res)
                        {

                            emailInsertData = _.cloneDeep(queryData);
                            emailInsertData.type=1;

                                emailInsertData.seen = 2; // seen 2 is sent

                            insertToDatabase(emailInsertData);
                        }).catch(()=>{
                            emailInsertData = _.cloneDeep(queryData);
                            emailInsertData.type=1;
                            emailInsertData.seen = 3; // Seen 3 is email unsent
                            insertToDatabase(emailInsertData);
                        });


                queryData.seen = 0; //  seen 0 unseen
                queryData.type = 2; //  Notification
               insertToDatabase(queryData);

                return res.json({msg:"Insert Successful"});

        }
    )
    
}
/**
 * Insert Data into Database .
 * @constructor
 * @param {object} data - data contains msg,to_user,from_user,type,seen
 */
function insertToDatabase(data)
{
    let finalData = 
    {
        from_user : data.from_user,
        to_user : data.to_user,
        msg : data.msg,
        type : data.type,
        seen : data.seen
    }

    Notification.create(finalData)
    .then(
        function success(result)
            {
                console.log(result);
                // res.send("Done");
            } ,
        function fail(err)
            {
                console.log(err);
                // res.send("Some thing broken");
            });
}

/**
 * Update Status of a notification.
 * @constructor
 * @param {object} req - request
 * @param {object} res - response
 */
function updateNotificationStatus(req,res)
{
    let queryData={}; 
    queryData.status = 1;
    user_id = req.params.user_id;
    noti_id = req.params.noti_id;
    updateAttribute(noti_id,queryData.status,"seen")
      .then(function(rowsUpdated) 
      {
        if(rowsUpdated>0)
            return res.json({msg: "message seen"})
        
        else
        {
            res.status(400);
            return res.json({msg:"notification does not exists"})    
        }
    })
    .catch(function(err)
    {
        console.log(err);
        res.status(500);
        return res.json({msg:"Error in Database"});
    });
}


/**
 * Update target attribute of a notification in Database.
 * @constructor
 * @param {object} noti_id - Notification Id to be updated
 * @param {object} code - Code to which updated
 * @param {object} target - Attribute to be updated
 * 
 */
function updateAttribute(noti_id,code,target)
{
    return Notification.update(
        {target: code},
        {where:{
            'id':[noti_id]
        } }
      );
}
/**
 * Delete Notification.
 * @constructor
 * @param {object} req - request
 * @param {object} res - response
 */
function deleteNotifications(req,res)
{
    let userId = req.params.user_id;
    let noti_id =req.params.noti_id ;
    

    updateAttribute(noti_id,1,user).then(function(rowsUpdated) {
        console.log(rowsUpdated)
        if(rowsUpdated>0)
            res.send("deleted successfuly")
        else
            res.send("record not found")    
        },
      function error(err)
      {
          console.log(err)
          res.send("record not found")
      });
     
}



module.exports = {showNotifications,addNotification,updateNotificationStatus,deleteNotifications};